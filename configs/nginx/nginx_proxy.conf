upstream c9 { server localhost:8181; }
upstream nodered { server localhost:1880; }
upstream netdata { server localhost:19999; }
upstream ungit { server localhost:8448; }
upstream jupyter { server localhost:8888; }
upstream slides { server localhost:8000; }

server {
    listen 80;
    server_name $HOSTNAME;

    #---------------------------------------------
    # c9                                         |
    #---------------------------------------------
    location / {
        proxy_pass http://c9;
    }

    #---------------------------------------------
    # Node-RED                                   |
    #---------------------------------------------
    location /node-red {
        proxy_pass http://nodered/node-red;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    location /api {
        proxy_pass http://nodered/api;
    }

    #---------------------------------------------
    # Netdata                                    |
    #---------------------------------------------
    location = /netdata {
        return 301 /netdata/;
    }

    location ~ /netdata/(?<ndpath>.*) {
        proxy_redirect off;
        proxy_set_header Host $host;

        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_http_version 1.1;
        proxy_pass_request_headers on;
        proxy_set_header Connection "keep-alive";
        proxy_store off;
        proxy_pass http://netdata/$ndpath$is_args$args;

        gzip on;
        gzip_proxied any;
        gzip_types *;
    }
    
    #---------------------------------------------
    # Ungit                                      |
    #---------------------------------------------
    location /ungit {
        proxy_pass http://ungit/ungit;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
    
    #---------------------------------------------
    # Jupyter                                    |
    #---------------------------------------------
    location /notebook/ {
        proxy_pass http://jupyter;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
    }

    location ~/notebook/* {
        proxy_pass http://jupyter;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;

        # WebSocket support
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 86400;
    }
    
    location /slides {
        proxy_pass http://slides/slides;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}