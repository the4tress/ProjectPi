These are instructions on how to configure a Raspberry Pi for various projects.

# Requirements

You will need at least:
- Raspberry Pi (any version)
- 4GB MicroSD Card (64GB recommended)
- [Latest Raspbian](https://www.raspberrypi.org/downloads/raspbian/) (Raspbian Lite recommended)
- [Etcher](https://etcher.io/)

# Installation

Use Etcher to burn the Raspbian Lite to the SD card.

Put the card in the Pi and connect keyboard, HDMI, ethernet (if available) and power.

> **Note:** The default username and password are `pi` and `raspberry`.

Use `raspi-config` to:

1. Change all of the locale information (there are 4 sections)
2. Enable SSH
3. Configure Wifi
4. Change the default user password
5. Update Hostname (I use `projectpi` as the hostname)

I recommend doing step 1 first because some characters are different on an American keyboard and it will mess up your passwords.

    sudo raspi-config

Do not update the device from this menu. We will update it later.

Exit the tool

Get the IP

    hostname -I

You now have a "headless" Raspberry Pi. You no longer need a monitor or keyboard.

SSH into the Pi from another machine and complete the remaining steps.

----

# Continued

After you SSH in from another machine, continue with these steps

# Configure Proxy (optional)

If you only wan to proxy your apt data, you can use this:

    sudo echo 'Acquire::http::Proxy "http://user:password@proxy.server:port/";' > /etc/apt/apt.conf.d/05proxy && sudo nano /etc/apt/apt.conf.d/05proxy

If you want to proxy everything then use this:

    echo 'http_proxy="http://user:password@proxy.server:port"' >> ~/.bashrc

# Install Basics

Get updates and install basics

    sudo apt-get update
    sudo apt-get upgrade -y

Install basic tools

    sudo apt-get install -y htop iftop tmux ncdu vim git nginx

Install build files

    sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev openssl bzip2 perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python autoconf autoconf-archive autogen automake python-pymongo python-yaml uuid-dev


Install `nvm`

If you want to use `node` you should use `nvm` (NodeJS Version Manger) to manage it. With `nvm` you can install several copies of node and avoid permission issues.

Clone the source and update .bashrc

    git clone https://github.com/creationix/nvm.git ~/.nvm
    echo "source ~/.nvm/nvm.sh" >> ~/.bashrc


Install `pyenv`

If you want to use `python` you should use `pyenv` to manage it. 

    curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
    echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc
    echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc


Install `docker`

Download and install docker

    curl -sSL https://get.docker.com | sh

The user needs to be added to the docker group

    sudo usermod -aG docker ${USER}

Now we have updated several things that require a reboot. Restart the Pi and continue after you log in again.

    sudo reboot

----

# Continued

After the Pi boots back up and you log in again (remember you updated your password) now we can start installing the fun stuff. Pick and choose what you want to install from the following instructions but update the `nginx` config file in `/etc/nginx/sites-available/dev` accordingly. Some apps are in there, some aren't.

## Install Docker-Compose

Download and compile docker-compose

    git clone https://github.com/docker/compose.git
    cd compose
    git checkout release
    cp -i Dockerfile Dockerfile.armhf
    sed -i -e 's/^FROM debian\:/FROM armhf\/debian:/' Dockerfile.armhf
    sed -i -e 's/x86_64/armel/g' Dockerfile.armhf
    docker build -t docker-compose:armhf -f Dockerfile.armhf .

This will take a while.

    docker run --rm --entrypoint="script/build/linux-entrypoint" -v $(pwd)/dist:/code/dist -v $(pwd)/.git:/code/.git "docker-compose:armhf"

Now you have a newly compiled `docker-compose`, now move it so it can be used.

    sudo cp dist/docker-compose-Linux-armv7l /usr/local/bin/docker-compose
    sudo chown root:root /usr/local/bin/docker-compose
    sudo chmod 0755 /usr/local/bin/docker-compose
    docker-compose version

Install autocomplete

    sudo curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

Cleanup

    cd ~
    rm -rf compose


## Install NodeJS and npm

We will use `nvm` to install the `node` v8.11.3 since I have had the best results with that version.

    nvm install v8.11.3

Now you have `node` installed.


## Install Python and pip

`pyenv` is the easiest way to install `python`. Install Python 3.7.0. If it fails, reboot and try again.

> **Note:** This takes a **LONG** time on a Raspberry Pi. It can take over an hour on a RasPi Zero. You may want to do this last... before bed.

    pyenv install 3.7.0 

Use 3.7.0 by default globally

    pyenv global 3.7.0

If you have time I would also install 2.7.13. This will also take a **LONG** time, but its good to have it already built when you need it.

    pyenv install 2.7.13


## Configure Nginx

Clone my repo with `nginx` configs, update and copy them, then enable `nginx`.

    cd ~
    git clone https://gitlab.com/the4tress/ProjectPi.git
    sed -i -- "s/\$HOSTNAME/$HOSTNAME/g" ProjectPi/configs/nginx/nginx_proxy.conf
    sudo cp ProjectPi/configs/nginx/nginx_proxy.conf /etc/nginx/conf.d/dev.conf
    sudo rm /etc/nginx/sites-enabled/default
    sudo systemctl enable nginx

> **Note:** Your `nginx` configs are now located in `/etc/nginx/conf.d/dev.conf`. Remember that if you need to change them based on what you install later in these instructions.


## Install Webmin

If you want to be able to easily manage the system from a web interface, install this.

> **Note:** I do *not* recommend installing this on anothing other than a Raspberry Pi 3 or newer. It can be resource intenseive.

    wget http://prdownloads.sourceforge.net/webadmin/webmin_1.881_all.deb
    sudo dpkg --install webmin_1.881_all.deb
    rm webmin_1.881_all.deb

`webmin` is now running, configured to autostart, and available at https://projectpi:10000/.


## Install Cloud9

`c9` is the default interface and should not be skipped. It is the easiest way to interact with the Pi.

    cd ~
    git clone https://github.com/c9/core.git .c9sdk
    cd .c9sdk
    scripts/install-sdk.sh
    cd ~

`c9` is *not* running yet, but when it is it will be available at http://projectpi:8181/

## Install Node-RED

`node-red` is by far the best tool when working with a Raspberry Pi. With `c9` and node-red together its becomes a very powerful platform very quickly.

    npm install -g --unsafe-perm node-red

```this isn't true yet. need to add settings.conf for node-red```
`node-red` is not running yet, but when it is it will be available at http://projectpi:1880/node-red and it's endpoints at http://projectpi:1880/api.


## Install Netdata

`netdata` provides a ton of stats about the system and allows you to quickly diagnose issues with the system.

    bash <(curl -Ss https://my-netdata.io/kickstart.sh)

`netdata` is now installed and configured to start at boot. It is available at http://projectpi:19999


## Install ungit

`ungit` allows you to keep track of changes in git repositories. It's very handy when working with complex branches and multiple authors.

    npm install -g ungit


## Install ino

`ino` allows yout to itneract with Arduino boards. It can compile and upload code from the pi to something like an ESP8266. Recommended with Python 3.7.0.

    pip install ino

> **Note:** This might be chainging to platformio (`pip install platformio -U`). Should be OK to install both if you want to play with them.


## Install Jupyter

`jupyter` is the best way to learn `python` quickly.

    pip install jupyter[notebook]


## Install pm2

`pm2` (Process Manager 2) is a tool to easily manage processes. These are things that you want to keep running in the background and automatically start when the Raspberry Pi powers on.

    npm install -g pm2

Make it so `pm2` automatically starts on reboot.

    pm2 startup

It tells you to run this command. Make sure you do that.

    sudo env PATH=$PATH:/home/pi/.nvm/versions/node/v8.11.3/bin /home/pi/.nvm/versions/node/v8.11.3/lib/node_modules/pm2/bin/pm2 startup systemd -u pi --hp /home/pi

Start `c9` and `node-red` with `pm2`

    pm2 start ~/.c9sdk/server.js --name=c9 -- -w /home/pi
    pm2 start node-red -- --settings ~/ProjectPi/configs/node-red/settings.js

Now those apps are running with `pm2`. If you want to stop one, just use `stop` in the command instead. When you have the services running that you want, `save` it.

    pm2 save

Now when you restart the Pi it will automatically start those saved processes.

    sudo reboot

Now you can get to:

- `c9` at http://projectpi/
- `node-red` at http://projectpi/node-red
- `node-red` endpoints at http://projectpi/api
- `webmin` at https://projectpi:10000/